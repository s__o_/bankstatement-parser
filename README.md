# Bank Statement Parser

This project helps to automatically parse bank statements from PDF files into a machine-readable JSON format. To this end, the project provides a REST API, to upload single bank statements to extract all transactions, including: date, description and value.
Further, a brief summary (number of transactions and balance) is provided.

Unlike other approaches, this approach relies on extracting transactions with the help of regular expressions and not based on (optical) table detection, as those tend to provide unreliable results.

> :warning: This project uses the `pdftotext` package that needs to be locally installed.

> :construction: In the current version, the project is restricted to [DKB](https://dkb.de) bank statements only.


## Configuration

To run the application, a configuration file is required. Create a new dotenv-file at `src/config/.env` containing the following information:

```
# tmp file details
TMP=./tmp   # folder to store files temporarily
TMP_FILE=tmp_file # name of tmp file
```

## Documentation

An overview about the API's structure and availible endpoints can be found in [./src/config/swagger.json](./src/config/swagger.json).

## Setup

### Installation

```bash
pip install -e .
```

### Run

```bash
python src/app.py
```

After that the API will be running on [http://0.0.0.0:5000/](http://0.0.0.0:5000/)