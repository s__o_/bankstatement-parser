from flask import redirect
import connexion

app = connexion.App(__name__, specification_dir='./')
app.add_api('./config/swagger.json')

@app.route('/')
def home():
    return redirect('/api/ui', 303)


if __name__ == '__main__':
    app.run(debug=True)
