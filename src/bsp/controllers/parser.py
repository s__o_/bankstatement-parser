import os
from flask import request, jsonify
from werkzeug.utils import secure_filename
from functools import reduce
from dotenv import dotenv_values


from typing import List
from bsp.models.trans import BTransaction
from bsp.services import dkb_parser


def import_file():
    """Entry point to upload, read, and extract bankstatements."""

    config = dotenv_values("./src/config/.env")

    file = request.files["pdf_file"]

    file_type = file.filename.split(".")[-1]
    if not os.path.exists(config["TMP"]):
        os.makedirs(config["TMP"])

    file.save(
        "/".join(
            [
                config["TMP"],
                secure_filename(".".join([config["TMP_FILE"], file_type])),
            ]
        )
    )
    file.close()

    transactions: List[BTransaction]
    transactions = dkb_parser.parse_file(config)

    trans_sum = reduce(
        lambda a, b: a + b, map(lambda t: t.value, transactions)
    )

    os.remove(f"{config['TMP']}/{config['TMP_FILE']}.pdf")

    return jsonify(
        info={"transactions": len(transactions), "sum": trans_sum},
        transactions=transactions,
    )
