import re
import os
from datetime import datetime
from typing import List

from bsp.models.trans import BTransaction


def is_end(string: str) -> List[BTransaction]:
    res = re.search(".Seite (\d+) von (\d+)$", string)
    if res is not None or "ALTER KONTOSTAND" in string:
        return True
    else:
        return False


def is_header(string: str):
    res = re.search("^Bu.*Wert", string.strip())
    return res is not None


def new_trans(string: str):
    string = string.lstrip()

    res = re.search(
        "^(\d{2}){1}[.](\d{2})[.]\s*(\d{2}){1}[.](\d{2})[.]", string
    )
    return res is not None


def get_trans_value(string: str):
    string = string.strip()
    res = re.search("([1234567890.]*)[,]([0123456789]{2})$", string)
    value = int(res.groups()[0].replace(".", "")) * 100 + int(res.groups()[1])

    return value


def get_trans_date(string: str, year: int):
    string = string.strip()
    res = re.search("^(\d{2}){1}[.](\d{2})[.]", string).group(0)

    date = datetime.strptime(res + str(year), "%d.%m.%Y")

    return date


def get_trans_desc(string: str):
    string = string.strip()
    start = re.search(
        "(\d{2}){1}[.](\d{2})[.]\s*(\d{2}){1}[.](\d{2})[.]", string
    )
    stop = re.search("(\d*){1}[,](\d{2})$", string)

    # print(start)
    return string[start.end() + 1 : stop.start() - 1].strip()


def convert_to_txt(config: dict):
    os.system(f"pdftotext -layout {config['TMP']}/{config['TMP_FILE']}.pdf")


def delete_file(config: dict):
    os.remove(f"{config['TMP']}/{config['TMP_FILE']}.txt")


def extract_transactions(config: dict):

    file = open(f"{config['TMP']}/{config['TMP_FILE']}.txt", "r")
    Lines = file.readlines()

    transactions = []

    year = None
    trans = False
    desc = ""
    date = ""
    value = ""

    for row in Lines:
        if "Kontoauszug Nummer" in row:
            year = int(re.search("(\d{4})", row).group(0))

        if is_header(row):
            new_table = True
            trans = True
            header_len = len(row)
        elif new_trans(row):
            if len(desc.strip()) > 0:
                transactions.append(BTransaction(date, desc, value))

            trans = True
            new_table = False
            date = get_trans_date(row, year)
            value = get_trans_value(row)
            desc = get_trans_desc(row)

            if len(row) < header_len:
                value = value * -1
            else:
                pass

        elif is_end(row):
            trans = False
        else:
            if trans:
                desc = desc + " " + row.strip()

    transactions.append(BTransaction(date, desc, value))

    return transactions


def parse_file(config: dict) -> List[BTransaction]:
    convert_to_txt(config)
    transactions = extract_transactions(config)
    delete_file(config)

    return transactions
