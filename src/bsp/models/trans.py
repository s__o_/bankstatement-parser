from dataclasses import dataclass
from datetime import datetime


@dataclass
class BTransaction:

    date: datetime
    desc: str
    value: int
